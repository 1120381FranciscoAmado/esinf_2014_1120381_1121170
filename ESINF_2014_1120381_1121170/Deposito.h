/* 
 * File:   Deposito.h
 * Author: Francisco Amado, 1120381 e Luciano Dias, 1121170
 * 
 * Created on 13 de Outubro de 2014, 18:19
 */

#ifndef DEPOSITO_H
#define	DEPOSITO_H

#include <iostream>
#include <sstream>

using namespace std;

class Deposito {
private:
    int chave;
    int num_paletes;
    int cap_max_paletes;
    int idProduto;

public:
    Deposito();
    Deposito(int chave, int num_paletes, int cap_max_paletes);
    Deposito(const Deposito& d);
    virtual ~Deposito();

    int getChave() const;
    int getNum_Paletes() const;
    int getCap_Max_Paletes() const;
    int getIdProduto() const;
    
    virtual int getTotalProdutos() const;

    void setCap_Max_Paletes(int cap_max_paletes);

    void incrementaIdProduto();

    virtual const Deposito& operator=(const Deposito& d);

    virtual void write(ostream &o);
};

Deposito::Deposito() {
    this->idProduto = 0;
}

Deposito::Deposito(int chave, int num_paletes, int cap_max_paletes) {
    this->chave = chave;
    this->num_paletes = num_paletes;
    this->cap_max_paletes = cap_max_paletes;
    this->idProduto = 0;
}

Deposito::Deposito(const Deposito& d) {
    this->chave = d.chave;
    this->num_paletes = d.num_paletes;
    this->cap_max_paletes = d.cap_max_paletes;
    this->idProduto = 0;
}

Deposito::~Deposito() {
}

int Deposito::getChave() const {
    return chave;
}

int Deposito::getNum_Paletes() const {
    return num_paletes;
}

int Deposito::getCap_Max_Paletes() const {
    return cap_max_paletes;
}

int Deposito::getIdProduto() const {
    return idProduto;
}

int Deposito::getTotalProdutos() const{
    return 0;
}

void Deposito::setCap_Max_Paletes(int cap_max_paletes) {
    this->cap_max_paletes = cap_max_paletes;
}

void Deposito::incrementaIdProduto() {
    idProduto++;
}

const Deposito& Deposito::operator=(const Deposito& d) {
    cap_max_paletes = d.getCap_Max_Paletes();
    chave = d.getChave();
    idProduto = d.getIdProduto();
    num_paletes = d.getNum_Paletes();
    return *this;
}

void Deposito::write(ostream &out) {
    out << "Deposito" << getChave() << endl;
}

ostream & operator<<(ostream &out, Deposito &d) {
    d.write(out);
    return out;
}


#endif	/* DEPOSITO_H */

