/* 
 * File:   main.cpp
 * Author: Francisco Amado, 1120381 e Luciano Dias, 1121170
 *
 * Created on 13 de Outubro de 2014, 10:48
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <time.h>
#include <fstream>

#include "Armazem.h"
#include "menu.h"

using namespace std;

void generateArmazem(Armazem * arm) {
    int num_dep_f, num_dep_n, num_pal_f, num_pal_n;

    /* initialize random seed: */
    srand(time(NULL));

    num_dep_f = rand() % 10 + 1;
    num_dep_n = rand() % 10 + 1;
    num_pal_f = rand() % 10 + 1;
    num_pal_n = rand() % 10 + 1;

    arm = new Armazem(num_dep_f, num_dep_n, num_pal_f, num_pal_n);
}

void askVariosFrescos(Armazem * arm) {
    string temp;
    do {
        cout << "Nome do produto (0 para sair)?   ";
        cin >> temp;
        if (temp != "0") {
            arm->InserirFresco(temp);
        }
        cout << endl;
    } while (temp != "0");
}

void askVariosNormal(Armazem * arm) {
    string temp;
    do {
        cout << "Nome do produto (0 para sair)?   ";
        cin >> temp;
        if (temp != "0") {
            arm->InserirNormal(temp);
        }
        cout << endl;
    } while (temp != "0");
}

void eliminarFresco(Armazem * arm) {
    arm->EliminarFresco();
}

void eliminarNormal(Armazem * arm) {
    arm->EliminarNormal();
}

void menuGrafo(Armazem * arm) {
    int resp;
    string nome;
    int d1 = 0, d2 = 0;

    do {
        if (arm->emptyGraph()) {
            cout << "ATENCAO: Ainda nao tem um grafo criado para este armazem" << endl;
        }
        resp = createGrafoMenu();
        switch (resp) {
            case 1:
                arm->criarGrafo();
                cout << "Criado o grafo aleatoriamente" << endl;
                break;
            case 2:
                if (!arm->emptyGraph()) {
                    d1 = askVertex(1);
                    d2 = askVertex(2);
                    arm->apresentarTodosPossiveis(d1, d2);
                } else {
                    cout << "Grafo vazio";
                }
                break;
            case 3:
                if (!arm->emptyGraph()) {
                    d1 = askVertex(1);
                    d2 = askVertex(2);
                    arm->percursoMaisCurtoTipo(d1, d2);
                } else {
                    cout << "Grafo vazio";
                }
                break;
            case 4:
                if (!arm->emptyGraph()) {
                    d1 = askVertex(1);
                    d2 = askVertex(2);
                    arm->percursoMaisCurto(d1, d2);
                } else {
                    cout << "Grafo vazio";
                }
                break;
            case 5:
                arm->printGraph();
        }
        cout << endl;
    } while (resp != 0);
}

Armazem* createRandomArmazem() {
    int num_dep_f, num_dep_n, num_pal_f, num_pal_n;

    /* initialize random seed */
    srand(time(NULL));

    num_dep_f = rand() % 10 + 1;
    num_dep_n = rand() % 10 + 1;
    num_pal_f = rand() % 10 + 1;
    num_pal_n = rand() % 10 + 1;

    return new Armazem(num_dep_f, num_dep_n, num_pal_f, num_pal_n);
}

void createArmazem(Armazem * arm) {
    int resp;
    string nome;
    vector<string> nomes;
    do {
        resp = createArmazemMenu();
        switch (resp) {
            case 1:
                cout << (*arm) << endl;
                break;
            case 2:
                nome = askNome();
                arm->InserirFresco(nome);
                break;
            case 3:
                nome = askNome();
                arm->InserirNormal(nome);
                break;
            case 4:
                askVariosFrescos(arm);
                break;
            case 5:
                askVariosNormal(arm);
                break;
            case 6:
                eliminarFresco(arm);
                break;
            case 7:
                eliminarNormal(arm);
                break;
            case 8:
                nome = askNomeFicheiro();
                arm->printTxt(nome);
                break;
            case 9:
                menuGrafo(arm);
        }
        cout << endl;
    } while (resp != 0);
}

/**
 * Metodo para leitura do ficheiro de dados do armazem
 * O armazem tem a estrutura de depositos e a seguir a estrutura do grafo:
 * Tipo;Chave;Cap_Max_Paletes;Num_paletes
 * ...
 * VOrigin;VDestination;EContent
 * ...
 * 
 * @return true caso haja sucesso em ler do ficheiro
 * 
 * FALTA ACABAR
 */
bool readFile(string nome, stack<string>& depF, stack<string>& depN, stack<string>& graph) {
    string line;
    char delim = ';'; //delimitador dos dados de cada deposito
    string filename = nome + ".txt";

    ifstream myfile(filename);

    if (myfile.is_open()) {
        getline(myfile, line);
        while (getline(myfile, line)) { //percore todas as linhas do ficheiro que correspondem aos depositos
            if (line.compare("VOrigin;VDestination;EContent") == 0) {
                break;
            }

            string tipo = line.substr(0, line.find(delim));
            line.erase(0, line.find(delim) + 1);

            if (tipo.compare("Fresco") == 0) {
                depF.push(line);
            } else if (tipo.compare("Normal") == 0) {
                depN.push(line);
            }
        }

        while (getline(myfile, line)) { //percore todas as restantes linhas do ficheiro que correspondem a estrutura do grafo
            graph.push(line);
        }
        myfile.close();
    } else {
        cout << "Impossivel de abrir o ficheiro, " << endl;
        return false;
    }
    return true;

}

Armazem* importArmazem() {
    string nome = askNomeFicheiro();
    stack<string> depF, depN, graph;

    bool flag = readFile(nome, depF, depN, graph);

    if (flag) {
        int iz = depN.size();
        cout << "Importados os dados do ficheiro " << nome << endl;
        return new Armazem(depF, depN, graph);
    } else {
        cout << "Impossivel de importar os dados" << endl;
        return NULL;
    }
}

int main() {

    int resp;
    do {
        resp = printMenuINICIAL();
        Armazem * arm;
        switch (resp) {
            case 1:
                arm = createRandomArmazem();
                createArmazem(arm);
                break;
            case 2:
                arm = importArmazem();
                createArmazem(arm);
                break;
            case 3:
                printInfo();
                break;
        }
        arm = NULL;
        delete arm;
    } while (resp != 0);

    return 0;
}

