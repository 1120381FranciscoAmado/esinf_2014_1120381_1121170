/* 
 * File:   Armazem.h
 * Author: Francisco Amado, 1120381 e Luciano Dias, 1121170
 *
 * Created on 13 de Outubro de 2014, 10:52
 */

#ifndef ARMAZEM_H
#define	ARMAZEM_H

#include <iostream>
#include <vector>
#include <stack>
#include <typeinfo>
#include <fstream>

#include "DepositoFresco.h"
#include "DepositoNormal.h"
#include "graphStlPath.h"

#define cap_max_paletes 10

using namespace std;

class Armazem {
private:
    int num_depositos_normais;
    int num_depositos_frescos;
    vector<Deposito *> depositos;
    int actual;

    graphStlPath<int, int> graph;

public:
    Armazem();
    Armazem(int num_depositos_frescos, int num_depositos_normais, int num_paletes_frescos,
            int num_paletes_normais);
    Armazem(stack<string>& depF, stack<string>& depN, stack<string>& graph);
    ~Armazem();

    void InserirFresco(string nome);
    void EliminarFresco();
    void InserirNormal(string nome);
    void EliminarNormal();

    void criarGrafo();

    bool emptyGraph();
    void printTxt(string nome);
    void printGraph();

    void apresentarTodosPossiveis(int d1, int d2);
    void percursoMaisCurto(int d1, int d2);
    void preencherListaTipo(int d1, int d2, set<int> &vertip);
    void percursoMaisCurtoTipo(int d1, int d2);
    void write(ostream & out) const;
};

Armazem::Armazem() {
}

/**
 * Inicializar o Armazem com os valores aleatorios
 */
Armazem::Armazem(int num_depositos_frescos, int num_depositos_normais, int num_paletes_frescos, int num_paletes_normais) {
    this->actual = 0;
    this->num_depositos_frescos = num_depositos_frescos;
    this->num_depositos_normais = num_depositos_normais;

    for (int i = 0; i < num_depositos_frescos; i++) {
        DepositoFresco * depF = new DepositoFresco(++actual, num_paletes_frescos, cap_max_paletes);
        depositos.push_back(depF);
    }
    for (int i = 0; i < num_depositos_normais; i++) {
        DepositoNormal * depN = new DepositoNormal(actual, num_paletes_normais, cap_max_paletes);
        depositos.push_back(depN);
    }
}

/**
 * Inicializar o Armazem com os valores/strings retirados do ficheiro
 */
Armazem::Armazem(stack<string>& depF, stack<string>& depN, stack<string>& graph) {
    int next = 0;
    string line;
    int sizef = depF.size(), sizen = depN.size(), sizeg = graph.size();
    char delim = ';'; //delimitador dos dados de cada deposito

    while (!depF.empty()) {
        line = depF.top();

        next = line.find(delim);
        string chave_s = line.substr(0, next);
        int chave = atoi(chave_s.c_str());
        line.erase(0, next + 1);

        next = line.find(delim);
        string cap_paletes_s = line.substr(0, next);
        int cap_paletes = atoi(cap_paletes_s.c_str());
        line.erase(0, next + 1);

        next = line.find(delim);
        string num_paletes_s = line.substr(0, next);
        int num_paletes = atoi(num_paletes_s.c_str());
        line.erase(0, next + 1);

        DepositoFresco * depFresco = new DepositoFresco(chave, num_paletes, cap_paletes);
        depositos.push_back(depFresco);

        depF.pop();
    }

    while (!depN.empty()) {
        line = depN.top();

        next = line.find(delim);
        string chave_s = line.substr(0, next);
        int chave = atoi(chave_s.c_str());
        line.erase(0, next + 1);

        next = line.find(delim);
        string cap_paletes_s = line.substr(0, next);
        int cap_paletes = atoi(cap_paletes_s.c_str());
        line.erase(0, next + 1);

        next = line.find(delim);
        string num_paletes_s = line.substr(0, next);
        int num_paletes = atoi(num_paletes_s.c_str());
        line.erase(0, next + 1);

        DepositoNormal * depNornal = new DepositoNormal(chave, num_paletes, cap_paletes);
        depositos.push_back(depNornal);

        depN.pop();
    }

    while (!graph.empty()) {
        line = graph.top();

        next = line.find(delim);
        int vorigin = atoi(line.substr(0, next).c_str());
        line.erase(0, next + 1);

        next = line.find(delim);
        int vdestination = atoi(line.substr(0, next).c_str());
        line.erase(0, next + 1);

        next = line.find(delim);
        int econtent = atoi(line.substr(0, next).c_str());
        line.erase(0, next + 1);

        this->graph.addGraphEdge(econtent, vorigin, vdestination);

        graph.pop();
    }
}

/**
 * Destrutor da memoria dinamica criada no armazem
 */
Armazem::~Armazem() {
    //eliminar cada um dos elementos do vector
    vector<Deposito*>::iterator killit;
    for (killit = depositos.begin(); killit != depositos.end(); killit++) {

        delete (*killit);
    }
}

/**
 * Insercao de um produto do tipo Fresco um deposito do tipo correspondente 
 * Estes sao guardados nas paletes por ordem de chegada e de forma a terem todas a mesma carga
 */
void Armazem::InserirFresco(string nome) {

    bool flag = false;

    vector<Deposito*>::iterator it;

    for (int i = 0; i != depositos.size(); i++) {
        flag = true;
        if (typeid (DepositoFresco) == typeid (*depositos[i])) {
            DepositoFresco * depF = (DepositoFresco*) depositos[i];
            if (depF->push(nome)) {
                break;
            }
        }
        flag = false;
    }

    if (flag) cout << "Produto inserido" << endl;

    if (!flag) cout << "Nao foi possivel inserir o produto" << endl;
}

/**
 * Insercao de um produto do tipo Normal um deposito do tipo correspondente 
 * Estes sao empilhados nas paletes de maior capacidade em primeiro lugar
 * 
 * FALTA ACABAR/REPENSAR
 */
void Armazem::InserirNormal(string nome) {

    bool flag = false;

    vector<Deposito*>::iterator it;

    for (int i = 0; i != depositos.size(); i++) {
        flag = true;
        if (typeid (DepositoNormal) == typeid (*depositos[i])) {
            DepositoNormal * depN = (DepositoNormal*) depositos[i];
            if (depN->push(nome)) {
                break;
            }
            flag = false;
        }
    }

    if (flag) cout << "Produto inserido" << endl;

    if (!flag) cout << "Nao foi possivel inserir o produto" << endl;
}

/**
 * Expedicao de um produto do tipo Fresco
 * Feita igualmente por ordem de chegada
 */
void Armazem::EliminarFresco() {
    for (int i = 0; i != depositos.size(); i++) {
        if (typeid (DepositoFresco) == typeid (*depositos[i])) {
            DepositoFresco * depF = (DepositoFresco*) depositos[i];
            if (depF->pop()) {
                return;
            } else {
                cout << "Impossivel eliminar um produto fresco (não existente)" << endl;
            }
        }
    }
}

/**
 * Expedicao de um produto do tipo Normal
 * Feita da forma inversa a insercao
 * 
 * FALTA ACABAR
 */
void Armazem::EliminarNormal() {
    vector<Deposito*>::iterator it;

    for (it = depositos.begin(); it != depositos.end(); it++) {
        if (typeid (DepositoNormal) == typeid (*it)) {
            DepositoNormal * depN = (DepositoNormal*) (*it);
            if (depN->pop()) {
                return;
            } else {
                cout << "Impossivel eliminar um produto normal (não existente)" << endl;
            }
        }
    }
}

/**
 * Criar o grafo a partir de valores aleatorios
 */
void Armazem::criarGrafo() {
    int dep1 = 0, dep2 = 0, max_depositos = depositos.size();

    /* initialize random seed: */
    srand(time(NULL));

    for (int i = 0; i < max_depositos; i++) {

        Deposito * deposito = depositos.at(i);

        int distancia1 = rand() % 10 + 1;
        int distancia2 = rand() % 10 + 1;

        //criar dois numeros aleatorios diferentes para criar as ligacoes
        do {
            dep1 = rand() % max_depositos;
            dep2 = rand() % max_depositos;
        } while ((dep1 == dep2) || (dep1 == i || dep2 == i) || (dep1 == 0 || dep2 == 0));

        //criar as ligacoes com os apontadores Deposito do vector
        Deposito * dep11 = depositos.at(dep1);
        graph.addGraphEdge(distancia1, deposito->getChave(), dep11->getChave());
        Deposito * dep22 = depositos.at(dep2);
        graph.addGraphEdge(distancia2, deposito->getChave(), dep22->getChave());
    }
}

/**
 * Verificar se o grafo do armazem se encontra vazio
 */
bool Armazem::emptyGraph() {
    return graph.empty();
}

/**
 * Guardar em ficheiro a estrutura do armazem (Depositos e as ligacoes entre si)
 */
void Armazem::printTxt(string nome) {

    string filename = nome + ".txt";

    ofstream myfile;
    myfile.open(filename);

    myfile << "Tipo;Chave;Cap_Max_Paletes;Num_paletes" << endl;
    for (int i = 0; i != depositos.size(); i++) {
        int key = 0;
        if (typeid (DepositoFresco) == typeid (*depositos[i])) {
            DepositoFresco * depF = (DepositoFresco*) depositos[i];
            myfile << "Fresco;" << depF->getChave() << ";" << depF->getCap_Max_Paletes() << ";" << depF->getNum_Paletes() << endl;
        }
        if (typeid (DepositoNormal) == typeid (*depositos[i])) {

            DepositoNormal * depN = (DepositoNormal*) depositos[i];
            myfile << "Normal;" << depN->getChave() << ";" << depN->getCap_Max_Paletes() << ";" << depN->getNum_Paletes() << endl;
        }
    }

    graph.writeTxt(myfile);

    myfile.close();

    cout << "Armazem impresso para o ficheiro " << filename << endl;
}

/**
 * Apresentar todos os percursos possiveis entre dois depositos
 * 
 * CONFIRMAR
 */
void Armazem::apresentarTodosPossiveis(int d1, int d2) {
    queue< stack<int> > caminhos = graph.distinctPaths(d1, d2);
    int i = 0;
    int sizequeue = caminhos.size();
    while (!caminhos.empty()) {
        stack<int> caminho = caminhos.front();
        cout << "Caminho " << ++i << ": ";
        while (!caminho.empty()) {

            cout << caminho.top() << ' ';
            caminho.pop();
        }
        cout << endl;
        caminhos.pop();
    }
}

/**
 * Calcular o percurso mais curto entre dois depositos
 * 
 * CONFIRMAR
 */
void Armazem::percursoMaisCurto(int d1, int d2) {
    list <int> path;
    bool flag = graph.caminhoMinimo(d1, d2, path);

    list <int>::iterator it;
    cout << "Caminho minimo entre os vertices " << d1 << " e " << d2 << endl;
    for (it = path.begin(); it != path.end(); it++) {
        cout << (*it) << ' ';
    }
    cout << endl;
}

void Armazem::preencherListaTipo(int d1, int d2, set<int> &vertip) {
    if (typeid (*depositos[d1]) == typeid (*depositos[d2])) {
        
        for (int it = 0; it != depositos.size(); it++) {
            if (typeid (depositos[it]) == typeid (*depositos[d1])) {
                vertip.insert(depositos[it]->getChave());
            }
        }
    }
}

/**
 * Calcular o percurso mais curto entre dois depositos do mesmo tipo
 * 
 * CONFIRMAR
 */

void Armazem::percursoMaisCurtoTipo(int d1, int d2) {
    list <int> path;
    set<int> vertip;
    preencherListaTipo(d1, d2, vertip);
    bool flag = graph.caminhoMinimoTipo(d1, d2, vertip, path);

    list <int>::iterator it;
    cout << "Caminho minimo entre os vertices do mesmo tipo " << d1 << " e " << d2 << endl;
    for (it = path.begin(); it != path.end(); it++) {
        cout << (*it) << ' ';
    }
    cout << endl;
}

/**
 * Imprimir o grafo para o ecra
 */
void Armazem::printGraph() {
    if (graph.empty()) {
        cout << "Grafo vazio. Impossivel imprimir" << endl;

        return;
    }
    cout << graph;
}

/**
 * Imprimir o Armazem para a stream de saida out
 */
void Armazem::write(ostream & out) const {
    out << "Armazem" << endl;

    if (this->depositos.size() == 0) {
        cout << "O armazem encontra-se vazio" << endl;
        return;
    }

    int depFresco = 0, paletesFresco = 0, depNormal = 0, paletesNormal = 0;
    int produtosFresco = 0, produtosNormal = 0;

    for (int i = 0; i != depositos.size(); i++) {
        if (typeid (DepositoFresco) == typeid (*depositos[i])) {
            DepositoFresco * depF = (DepositoFresco*) depositos[i];
            depFresco++;
            paletesFresco += depF->getNum_Paletes();
            produtosFresco += depF->getTotalProdutos();
        }
        if (typeid (DepositoNormal) == typeid (*depositos[i])) {

            DepositoNormal * depN = (DepositoNormal*) depositos[i];
            depNormal++;
            paletesNormal += depN->getNum_Paletes();
            produtosNormal += depN->getTotalProdutos();
        }
    }

    cout << "Numero total de depositos: " << depFresco + depNormal << endl;
    cout << "Depositos frescos:  " << depFresco << " numero total de paletes:    " << paletesFresco << "    total produtos: " << produtosFresco << endl;
    cout << "Depositos normais:  " << depNormal << " numero total de paletes:    " << paletesNormal << "    total produtos: " << produtosNormal << endl;
}

/**
 * Sobrecarga do operador << para impressao do Armazem
 */
ostream& operator<<(ostream& out, const Armazem & obj) {
    obj.write(out);
    return out;
}
#endif	/* ARMAZEM_H */