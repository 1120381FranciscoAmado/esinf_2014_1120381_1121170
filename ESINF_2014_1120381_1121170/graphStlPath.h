#ifndef _graphStlPath_
#define _graphStlPath_

#include <string.h> // memset include for g++
#include <stack>
#include <queue>
#include <bitset>
#include <type_traits>
#include <set>

using namespace std;

#include "graphStl.h"

template<class TV, class TE>
class graphStlPath : public graphStl <TV, TE> {
protected:
    void lengthFirstVisitRecursive(typename list < graphVertex <TV, TE> >::iterator itv, bitset <MAX_VERTICES> &taken, queue <TV> &q) const;
    void distinctPathsRecursive(typename list < graphVertex <TV, TE> >::iterator itvo, typename list < graphVertex <TV, TE> >::iterator itvd, bitset <MAX_VERTICES> &taken, stack <TV> &s, queue < stack <TV> > &qr);
    bool existeCaminho(typename list < graphVertex <TV, TE> >::iterator it1, typename list < graphVertex <TV, TE> >::iterator it2, bitset <MAX_VERTICES> &visitados);

public:
    graphStlPath();

    bool empty();

    bool conexo();
    bool caminhoMinimo(const TV &vi, const TV &vf, list <TV> &path);
    bool caminhoMinimoTipo(const TV &vi, const TV &vf, set <TV> &vertip, list <TV> &path);
    queue <TV> lengthFirstVisit(const TV &vContent);
    queue <TV> breadthFirstVisit(const TV &vContent);
    queue < stack <TV> > distinctPaths(const TV &vOrigin, const TV &vDestination);
    bool dijkstrasAlgorithm(const TV &vContent, vector <int> &pathKeys, vector <TE> &dist);
    queue <TV> getDijkstrasPath(int vKeyOrigin, vector <int> pathKeys);

    void writeTxt(ofstream& o);
};

template<class TV, class TE>
graphStlPath<TV, TE>::graphStlPath() : graphStl<TV, TE>() {
}

template<class TV, class TE>
void graphStlPath<TV, TE>::lengthFirstVisitRecursive(typename list < graphVertex <TV, TE> >::iterator itv, bitset <MAX_VERTICES> &taken, queue <TV> &q) const {
    taken[itv->getVKey()] = true;
    q.push(itv->getVContent());

    for (typename list < graphEdge <TV, TE> >::iterator ite = itv->getAdjacenciesBegin(); ite != itv->getAdjacenciesEnd(); ite++)
        if (!taken[ite->getVDestination()->getVKey()]) lengthFirstVisitRecursive(ite->getVDestination(), taken, q);
}

template<class TV, class TE>
queue <TV> graphStlPath<TV, TE>::lengthFirstVisit(const TV &vContent) {
    bitset <MAX_VERTICES> taken;
    queue < TV > q;

    typename list < graphVertex <TV, TE> >::iterator itv;
    if (!(graphStl<TV, TE>::getVertexIteratorByContent(itv, vContent))) return q;

    lengthFirstVisitRecursive(itv, taken, q);
    return q;
}

template<class TV, class TE>
queue <TV> graphStlPath<TV, TE>::breadthFirstVisit(const TV &vContent) {
    bitset <MAX_VERTICES> taken;
    queue <TV> qr;

    typename list < graphVertex <TV, TE> >::iterator itv;
    if (!(graphStl<TV, TE>::getVertexIteratorByContent(itv, vContent))) return qr;

    queue <typename list < graphVertex <TV, TE> >::iterator> qi;

    qi.push(itv);
    taken[itv->getVKey()] = true;

    while (!qi.empty()) {
        itv = qi.front();
        qr.push(itv->getVContent());

        for (typename list < graphEdge <TV, TE> >::iterator ite = itv->getAdjacenciesBegin(); ite != itv->getAdjacenciesEnd(); ite++)
            if (!taken[ite->getVDestination()->getVKey()]) {
                qi.push(ite->getVDestination());
                taken[ite->getVDestination()->getVKey()] = true;
            }
        qi.pop();
    }
    return qr;
}

/**
 * Metodo recursivo para preencher a queue qr com os caminhos encontrados
 * usa a stack s como auxiliar para ir preenchendo os vértices, não visitados e possíveis do caminho
 * 
 */
template<class TV, class TE>
void graphStlPath<TV, TE>::distinctPathsRecursive(typename list < graphVertex <TV, TE> >::iterator itvo, typename list < graphVertex <TV, TE> >::iterator itvd, bitset <MAX_VERTICES> &taken, stack <TV> &s, queue < stack <TV> > &qr) {
    if (itvo == itvd) {
        s.push(itvo->getVContent());
        qr.push(s);
        s.pop();
        return;
    }

    taken[itvo->getVKey()] = true;
    s.push(itvo->getVContent());

    for (typename list < graphEdge <TV, TE> >::iterator ite = itvo->getAdjacenciesBegin(); ite != itvo->getAdjacenciesEnd(); ite++) {
        if (!taken[ite->getVDestination()->getVKey()]) distinctPathsRecursive(ite->getVDestination(), itvd, taken, s, qr);
    }

    taken[itvo->getVKey()] = false;
    s.pop();
}

/**
 * Caminhos simples (nao repetidos) entre dois vertices
 * 
 * retorna queue com os caminhos existentes
 */
template<class TV, class TE>
queue < stack <TV> > graphStlPath<TV, TE>::distinctPaths(const TV &vOrigin, const TV &vDestination) {
    bitset <MAX_VERTICES> taken;
    stack < TV > s;
    queue < stack <TV> > qr;

    typename list < graphVertex <TV, TE> >::iterator itvo, itvd;
    if (!(graphStl<TV, TE>::getVertexIteratorByContent(itvo, vOrigin) && graphStl<TV, TE>::getVertexIteratorByContent(itvd, vDestination))) return qr;

    distinctPathsRecursive(itvo, itvd, taken, s, qr);
    return qr;
}

/**
 * Metodo para verificar se existe um caminho possivel entre dois vertices
 * 
 */
template<class TV, class TE>
bool graphStlPath<TV, TE>::existeCaminho(typename list < graphVertex <TV, TE> >::iterator it1, typename list < graphVertex <TV, TE> >::iterator it2, bitset <MAX_VERTICES> &visitados) {
    if (it1 == it2) return true;

    visitados[it1->getVKey()] = true;

    for (typename list < graphEdge <TV, TE> >::iterator ite =
            it1->getAdjacenciesBegin();
            ite != it1->getAdjacenciesEnd(); ite++) {
        typename list < graphVertex <TV, TE> >::iterator nit1 =
                ite->getVDestination();

        if (!visitados[nit1->getVKey()]) {
            bool out = existeCaminho(nit1, it2, visitados);

            if (out) return true;
        }
    }

    return false;
}

/**
 * Metodo para verificar se o grafo se encontra vazio
 */
template<class TV, class TE>
bool graphStlPath<TV, TE>::empty() {
    return graphStlPath::vlist.empty();
}

/**
 * Metodo para verificar se um grafo e conexo, se quaisquer que sejam os 
 * vértices distintos u e v existe sempre um caminho que os une
 */
template<class TV, class TE>
bool graphStlPath<TV, TE>::conexo() {
    bool cn = true;
    for (typename list < graphVertex <TV, TE> >::iterator it1 =
            graphStl<TV, TE>::vlist.begin();
            it1 != prev(graphStl<TV, TE>::vlist.end());
            it1++) {
        for (typename list < graphVertex <TV, TE> >::iterator it2 =
                next(it1);
                it2 != graphStl<TV, TE>::vlist.end();
                it2++) {

            bitset <MAX_VERTICES> b1, b2;
            if ((!existeCaminho(it1, it2, b1))&&(!existeCaminho(it2, it1, b2))) {
                cn = false;
                cout << (*it1).getVContent() << ", " << (*it2).getVContent() << endl;
            }
        }
    }
    return cn;
}

/**
 * Metodo para descobrir o caminho minimo entre dois vertices (versao simples
 * do algoritmo de Dijkstras
 */
template<class TV, class TE>
bool graphStlPath<TV, TE>::caminhoMinimo(const TV &vi, const TV &vf, list <TV> &path) {
    typename list < graphVertex <TV, TE> >::iterator iti, itf, itv;
    if (!graphStl<TV, TE>::getVertexIteratorByContent(iti, vi) ||
            !graphStl<TV, TE>::getVertexIteratorByContent(itf, vf)) return false;

    vector <int> pathKeys;
    vector <int> dist;
    queue <typename list < graphVertex <TV, TE> >::iterator> expansion;


    for (typename list < graphVertex <TV, TE> >::iterator it = graphStl<TV, TE>::vlist.begin();
            it != graphStl<TV, TE>::vlist.end(); it++) {
        pathKeys.push_back(-1);
        dist.push_back(graphStl<TV, TE>::getInfinite());
    }

    dist[iti->getVKey()] = 0;
    expansion.push(iti);

    while (!expansion.empty()) {
        itv = expansion.front();
        expansion.pop();

        for (typename list < graphEdge <TV, TE> >::iterator ite = itv->getAdjacenciesBegin();
                ite != itv->getAdjacenciesEnd(); ite++) {
            if (dist[ite->getVDestination()->getVKey()] == graphStl<TV, TE>::getInfinite()) {
                int keyOrig = itv->getVKey(), keyDest = ite->getVDestination()->getVKey();
                dist[keyDest] = dist[keyOrig] + 1;
                pathKeys[keyDest] = keyOrig;
                expansion.push(ite->getVDestination());
            }
        }
    }

    int vk = itf->getVKey();
    if (dist[vk] == graphStl<TV, TE>::getInfinite()) return false;



    TV el;
    path.clear();
    while (vk != -1) {
        this->getVertexContentByKey(el, vk);
        path.push_back(el);
        vk = pathKeys[vk];
    }
    path.reverse();
}

/**
 * Metodo para descobrir o caminho minimo entre dois vertices do mesmo Tipo
 */
template<class TV, class TE>
bool graphStlPath<TV, TE>::caminhoMinimoTipo(const TV &vi, const TV &vf, set <TV> &vertip, list <TV> &path) {
    typename list < graphVertex <TV, TE> >::iterator iti, itf, itv;
    if (!graphStl<TV, TE>::getVertexIteratorByContent(iti, vi) ||
            !graphStl<TV, TE>::getVertexIteratorByContent(itf, vf)) return false;

    vector <int> pathKeys;
    vector <int> dist;
    queue <typename list < graphVertex <TV, TE> >::iterator> expansion;


    for (typename list < graphVertex <TV, TE> >::iterator it = graphStl<TV, TE>::vlist.begin();
            it != graphStl<TV, TE>::vlist.end(); it++) {
        pathKeys.push_back(-1);
        dist.push_back(graphStl<TV, TE>::getInfinite());
    }

    dist[iti->getVKey()] = 0;
    expansion.push(iti);

    while (!expansion.empty()) {
        itv = expansion.front();
        expansion.pop();

        for (typename list < graphEdge <TV, TE> >::iterator ite = itv->getAdjacenciesBegin();
                ite != itv->getAdjacenciesEnd(); ite++) {
            if (dist[ite->getVDestination()->getVKey()] == graphStl<TV, TE>::getInfinite()) {
                int keyOrig = itv->getVKey(), keyDest = ite->getVDestination()->getVKey();
                if (vertip.find(keyOrig) != vertip.end()) {
                    dist[keyDest] = dist[keyOrig] + 1;
                    pathKeys[keyDest] = keyOrig;
                    expansion.push(ite->getVDestination());
                }
            }
        }
    }

    int vk = itf->getVKey();
    if (dist[vk] == graphStl<TV, TE>::getInfinite()) return false;



    TV el;
    path.clear();
    while (vk != -1) {
        this->getVertexContentByKey(el, vk);
        path.push_back(el);
        vk = pathKeys[vk];
    }
    path.reverse();
}

template<class TV, class TE>
bool graphStlPath<TV, TE>::dijkstrasAlgorithm(const TV &vContent, vector <int> &pathKeys, vector <TE> &dist) {
    vector <typename list < graphVertex <TV, TE> >::iterator> path;
    bitset <MAX_VERTICES> process; // Starts with all false

    pathKeys.clear();
    dist.clear();
    for (typename list < graphVertex <TV, TE> >::iterator it = graphStl<TV, TE>::vlist.begin(); it != graphStl<TV, TE>::vlist.end(); it++) {
        path.push_back(it);
        pathKeys.push_back(-1);
        dist.push_back(graphStl<TV, TE>::getInfinite());
    }

    typename list < graphVertex <TV, TE> >::iterator itv;
    if (!(graphStl<TV, TE>::getVertexIteratorByContent(itv, vContent))) return false;

    TE zeroValue;
    if ((is_integral<TE>::value) || (is_floating_point<TE>::value)) memset(&zeroValue, 0, sizeof (zeroValue)); //tipos primitivos

    dist[itv->getVKey()] = zeroValue; // Inicializa��o com valores nulos :	dist[itv->getVKey()] = 0;

    while (itv != graphStl<TV, TE>::vlist.end()) {
        process[itv->getVKey()] = true;
        for (typename list < graphEdge <TV, TE> >::iterator ite = itv->getAdjacenciesBegin(); ite != itv->getAdjacenciesEnd(); ite++) {
            int destKey = ite->getVDestination()->getVKey();
            if ((!process[destKey]) && dist[destKey] > dist[itv->getVKey()] + ite->getEContent()) {
                path[destKey] = itv;
                pathKeys[destKey] = itv->getVKey();
                dist[destKey] = dist[itv->getVKey()] + ite->getEContent();
            }
        }

        TE min = graphStl<TV, TE>::getInfinite();
        int vmin = -1;
        for (size_t i = 0; i < dist.size(); i++) {
            if (!process[i] && dist[i] < min) {
                min = dist[i];
                vmin = i;
            }
        }
        if (vmin == -1) itv = graphStl<TV, TE>::vlist.end();
        else graphStl<TV, TE>::getVertexIteratorByKey(itv, vmin);
    }

    return true;
}

template<class TV, class TE>
queue <TV> graphStlPath<TV, TE>::getDijkstrasPath(int vKeyOrigin, vector <int> pathKeys) {
    queue <TV> path;

    if (pathKeys[vKeyOrigin] != -1) {
        TV dest;
        getVertexContentByKey(dest, vKeyOrigin);

        int vi = vKeyOrigin;
        while (vi != -1) {
            getVertexContentByKey(dest, vi);
            path.push(dest);
            vi = pathKeys[vi];
        }
    }

    return path;
}

template<class TV, class TE>
void graphStlPath<TV, TE>::writeTxt(ofstream& out) {
    out << "VOrigin;VDestination;EContent" << endl;
    for (typename list < graphVertex <TV, TE> >::iterator itv = this->graphStl <TV, TE>::vlist.begin(); itv != this->graphStl <TV, TE>::vlist.end(); itv++) {
        for (typename list < graphEdge <TV, TE> >::iterator ite = itv->getAdjacenciesBegin(); ite != itv->getAdjacenciesEnd(); ite++) {
            out << itv->getVContent() << ';' << ite->getVDestination()->getVContent() << ';' << ite->getEContent() << endl;
        }
    }

    cout << "Imprimi!" << endl;
}


#endif