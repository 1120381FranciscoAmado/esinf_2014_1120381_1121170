/* 
 * File:   PaleteFresca.h
 * Author: Francisco Amado, 1120381 e Luciano Dias, 1121170
 * 
 * Created on 22 de Outubro de 2014, 17:17
 */

#ifndef PALETEFRESCA_H
#define	PALETEFRESCA_H

#include "Palete.h"
#include "Produto.h"
#include <queue>
#include <iostream>

using namespace std;

class PaleteFresca : public Palete {
private:
    queue<Produto*> paleteFresca;
public:
    PaleteFresca();
    PaleteFresca(int capacidademax, string nome);
    PaleteFresca(const Palete& p);
    ~PaleteFresca();

    bool push(string idProduto, string nome);
    void pop();

    int size() const;

    void write(ostream& out) const;
    friend ostream& operator<<(ostream& out, const PaleteFresca& pf);

};

PaleteFresca::PaleteFresca() {
}

PaleteFresca::PaleteFresca(int capacidademax, string nome) : Palete(nome, capacidademax) {
}

PaleteFresca::PaleteFresca(const Palete& d) : Palete(d) {
}

PaleteFresca::~PaleteFresca() {
}

bool PaleteFresca::push(string idProduto, string nome) {
    if (paleteFresca.size() < getCapacidadeMax()) {
        Produto * prod = new Produto(idProduto, nome);
        paleteFresca.push(prod);
        return true;
    }
    return false;
}

void PaleteFresca::pop() {
    cout << "Retirado o produto fresco: " << paleteFresca.front()->getNome() << endl;
    paleteFresca.pop();
}

int PaleteFresca::size() const {
    return paleteFresca.size();
}

void PaleteFresca::write(ostream& out) const {
    Palete::write(out);
}

ostream & operator<<(ostream &out, const PaleteFresca &pf) {
    pf.write(out);
    return out;
}

#endif	/* PALETEFRESCA_H */

