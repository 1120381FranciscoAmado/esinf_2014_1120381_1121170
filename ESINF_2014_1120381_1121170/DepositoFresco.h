/* 
 * File:   DepositoFrescos.h
 * Author: Francisco Amado, 1120381 e Luciano Dias, 1121170
 *
 * Created on 22 de Outubro de 2014, 17:14
 */

#ifndef DEPOSITOFRESCO_H
#define	DEPOSITOFRESCO_H
#include <vector>
#include <iostream>

#include "Deposito.h"
#include "PaleteFresca.h"

using namespace std;

class DepositoFresco : public Deposito {
private:
    vector<PaleteFresca> depFresco;
    vector<PaleteFresca>::iterator next;

public:
    DepositoFresco();
    DepositoFresco(int chave, int num_paletes, int cap_max_palete);
    DepositoFresco(const Deposito& d);
    DepositoFresco(const DepositoFresco& df);
    ~DepositoFresco();

    bool push(string nome);
    bool pop();

    int getTotalProdutos() const;

    bool operator==(const Deposito& df) const;
    const DepositoFresco& operator=(const DepositoFresco& df);


    void write(ostream& out) const;

};

DepositoFresco::DepositoFresco() {
}

DepositoFresco::DepositoFresco(int chave, int num_paletes, int cap_max_paletes) : Deposito(chave, num_paletes, cap_max_paletes) {
    for (int i = 0; i < num_paletes; i++) {
        string nome = "Fresca " + i;
        PaleteFresca * palF = new PaleteFresca(cap_max_paletes, nome);
        depFresco.push_back(*palF);
    }
    next = depFresco.begin();
}

DepositoFresco::DepositoFresco(const Deposito &d) : Deposito(d) {
}

DepositoFresco::~DepositoFresco() {
}

bool DepositoFresco::push(string nome) {
    incrementaIdProduto();
    stringstream ss;
    ss << getChave() << "_" << getIdProduto();

    if (next != depFresco.begin() && next != depFresco.end()) {
        next++;
    } else if (next == depFresco.end()) {
        next = depFresco.begin();
    }

    if (next->push(ss.str(), nome)) return true;
    return false;
}

bool DepositoFresco::pop() {
    if (depFresco.empty()) {
        return false;
    }
    depFresco.front().pop();
    return true;
}

int DepositoFresco::getTotalProdutos() const {
    int totalProdutos = 0;
    vector<PaleteFresca>::const_iterator itF;

    for (itF = depFresco.cbegin(); itF != depFresco.cend(); itF++) {
        totalProdutos += itF->size();
    }

    return totalProdutos;
}

bool DepositoFresco::operator==(const Deposito& dep) const {
    if (typeid (*this) == typeid (dep)) {
        if (Deposito::getChave() == dep.getChave()) {
            return true;
        }
    }
    return false;
}

const DepositoFresco& DepositoFresco::operator=(const DepositoFresco& dep) {
    (*this).Deposito::operator=(dep);
    return *this;
}

void DepositoFresco::write(ostream &out) const {
    out << "Deposito Fresco" << getChave() << endl;
}

ostream & operator<<(ostream &out, DepositoFresco &d) {
    d.write(out);
    return out;
}

#endif	/* DEPOSITOFRESCO_H */

