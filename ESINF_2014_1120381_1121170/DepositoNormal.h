/* 
 * File:   DepositoNormal.h
 * Author: Francisco Amado, 1120381 e Luciano Dias, 1121170
 *
 * Created on 22 de Outubro de 2014, 17:14
 */

#ifndef DEPOSITONORMAL_H
#define	DEPOSITONORMAL_H
#include <vector>
#include <iostream>

#include "Deposito.h"
#include "PaleteNormal.h"

using namespace std;

class DepositoNormal : public Deposito {
private:
    vector<PaleteNormal> depNormal;
    vector<PaleteNormal>::iterator next;
    bool imparesVazios;

public:
    DepositoNormal();
    DepositoNormal(int chave, int num_paletes, int cap_max_paletes);
    DepositoNormal(const Deposito& d);
    DepositoNormal(const DepositoNormal& dn);
    ~DepositoNormal();

    bool push(string nome);
    bool pop();

    int getTotalProdutos() const;

    bool operator==(const Deposito* dep) const;
    bool operator!=(const DepositoNormal& dn);
    DepositoNormal& operator=(const DepositoNormal& dn);

    void write(ostream& out) const;

};

DepositoNormal::DepositoNormal() {
}

DepositoNormal::DepositoNormal(int chave, int num_paletes, int cap_max_paletes) : Deposito(chave, num_paletes, cap_max_paletes) {
    imparesVazios = false;
    int actual = 0;
    for (int i = 0; i < num_paletes; i++) {
        stringstream nomess;
        nomess << "Normal " << i;

        string nome = nomess.str();

        actual = i;

        if (actual % 2 == 0) {
            PaleteNormal * palN = new PaleteNormal(actual, cap_max_paletes, nome);
            depNormal.push_back(*palN);
        } else {
            PaleteNormal * palN = new PaleteNormal(actual, cap_max_paletes / 2, nome);
            depNormal.push_back(*palN);
        }
    }

    next = depNormal.begin();
    if (next->getChave() % 2 != 0) { //
        next++;
    }
}

DepositoNormal::DepositoNormal(const Deposito& d) : Deposito(d) {
}

DepositoNormal::DepositoNormal(const DepositoNormal& dn) : Deposito(dn) {
}

DepositoNormal::~DepositoNormal() {
}

bool DepositoNormal::push(string nome) {
    incrementaIdProduto();
    stringstream ss;
    ss << getChave() << "_" << getIdProduto();
    if (next->size() == next->getCapacidadeMax()) { //a palete está cheia
        if (next == depNormal.end()) { //caso seja a ultima ou penultima
            next = depNormal.begin(); //inicia na primeira
            next++;
        } else if (next == depNormal.end() - 1) {
            next = depNormal.begin(); //inicia na primeira
        }
    }
    next += 2;
    if (next->push(ss.str(), nome)) return true;
    return false;
}

/**
 * FALTA ACABAR
 */
bool DepositoNormal::pop() {
    vector<PaleteNormal>::iterator it;

    for (it = depNormal.begin(); it != depNormal.end(); it++) {
        if (it->getChave() % 2 != 0) {
            if (it->empty() == false) {
                it->pop();
                return true;
            } else {
                cout << "Palete " << it->getChave() << "  Sem produtos para retirar" << endl;
            }
        }
    }

    for (it = depNormal.begin(); it != depNormal.end(); it++) {
        if (it->getChave() % 2 == 0) {
            if (it->empty() == false) {
                it->pop();
                return true;
            } else {
                cout << "Palete " << it->getChave() << "  Sem produtos para retirar" << endl;
            }
        }
    }

    return false;
}

int DepositoNormal::getTotalProdutos() const {
    int totalProdutos = 0;
    vector<PaleteNormal>::const_iterator itN;

    for (itN = depNormal.begin(); itN != depNormal.end(); itN++) {
        totalProdutos += itN->size();
    }

    return totalProdutos;
}

bool DepositoNormal::operator==(const Deposito* dep) const {
    if (typeid (*this) == typeid (*dep)) {
        if (Deposito::getChave() == dep->getChave()) {
            return true;
        }
    }
    return false;
}

void DepositoNormal::write(ostream &out) const {
    out << "DepositoNormal" << getChave() << endl;
}

ostream & operator<<(ostream &out, DepositoNormal &d) {
    d.write(out);
    return out;
}

#endif	/* DEPOSITONORMAL_H */

