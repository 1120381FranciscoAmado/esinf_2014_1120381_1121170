/* 
 * File:   Produto.h
 * Author: Francisco Amado, 1120381 e Luciano Dias, 1121170
 * 
 * Created on 22 de Outubro de 2014, 17:24
 */

#ifndef PRODUTO_H
#define	PRODUTO_H
#include <iostream>

using namespace std;

class Produto {
private:
    string id;
    string nome;
public:
    Produto();
    Produto(string id, string nome);
    Produto(const Produto &p);
    ~Produto();

    void setId(const string id);
    string getId() const;
    void setNome(const string& nome);
    string getNome() const;

    bool operator!=(const Produto& p) const;
    bool operator==(const Produto& p)const;
    const Produto & operator=(const Produto& p);

    void write(ostream &out) const;
};

Produto::Produto() {
}

Produto::Produto(string id, string nome) {
    this->nome = nome;
    this->id = id;
}

Produto::Produto(const Produto& p) {
    this->nome = p.nome;
    this->id = p.id;
}

Produto::~Produto() {
}

string Produto::getNome() const {
    return nome;
}

void Produto::setNome(const string& nome) {
    this->nome = nome;
}

string Produto::getId() const {
    return id;
}

void Produto::setId(const string id) {
    this->id = id;
}

bool Produto::operator==(const Produto& p) const {
    if (id == p.id) {
        return true;
    }
    return false;
}

bool Produto::operator!=(const Produto& p) const {
    if (id != p.id) {
        return true;
    }
    return false;
}

const Produto& Produto::operator=(const Produto& p) {
    id = p.id;
    nome = p.nome;
    return *this;
}

void Produto::write(ostream &out) const {
    out << "Nome: " << nome << "id: " << id << endl;
}

ostream& operator<<(ostream& out, const Produto& p) {
    p.write(out);
    return out;
}

#endif	/* PRODUTO_H */

