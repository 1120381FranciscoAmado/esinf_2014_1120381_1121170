/* 
 * File:   PaleteNormal.h
 * Author: Francisco Amado, 1120381 e Luciano Dias, 1121170
 *
 * Created on 22 de Outubro de 2014, 17:18
 */

#ifndef PALETENORMAL_H
#define	PALETENORMAL_H

#include <iostream>
#include <stack>

#include "Produto.h"
#include "Palete.h"

using namespace std;

class PaleteNormal : public Palete {
private:
    stack<Produto*> paleteNormal;
    int chave;

public:
    PaleteNormal();
    PaleteNormal(int chave, int capacidademax, string nome);
    PaleteNormal(const Palete& p);
    PaleteNormal(const PaleteNormal& pn);
    ~PaleteNormal();

    int getChave() const;

    bool push(string idProduto, string nome);
    void pop();

    int size() const;
    bool empty() const;

    void write(ostream& out) const;
    friend ostream& operator<<(ostream& out, const PaleteNormal& pn);

};

PaleteNormal::PaleteNormal() : Palete() {
}

PaleteNormal::PaleteNormal(int chave, int capacidadeMaxima, string nome) : Palete(nome, capacidadeMaxima) {
    this->chave = chave;
}

PaleteNormal::PaleteNormal(const Palete& d) : Palete(d) {
}

PaleteNormal::PaleteNormal(const PaleteNormal& pn) : Palete(pn) {
    this->chave = pn.chave;
    //this->paleteNormal(pn.paleteNormal);
}

PaleteNormal::~PaleteNormal() {
}

int PaleteNormal::getChave() const {
    return chave;
}

bool PaleteNormal::push(string idProduto, string nome) {
    if (paleteNormal.size() < getCapacidadeMax()) {
        Produto * prod = new Produto(idProduto, nome);
        paleteNormal.push(prod);
        return true;
    }
    return false;
}

void PaleteNormal::pop() {
    cout << "Retirado o produto fresco: " << paleteNormal.top()->getNome() << endl;
    paleteNormal.pop();
}

int PaleteNormal::size() const {
    return paleteNormal.size();
}

bool PaleteNormal::empty() const {
    return paleteNormal.empty();
}

void PaleteNormal::write(ostream& out) const {
    Palete::write(out);
}

ostream& operator<<(ostream &out, const PaleteNormal& pn) {
    pn.write(out);
    return out;
}
#endif	/* PALETENORMAL_H */

