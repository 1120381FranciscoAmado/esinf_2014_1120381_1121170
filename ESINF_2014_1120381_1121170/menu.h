/* 
 * File:   menu.h
 * Author: Francisco Amado, 1120381 e Luciano Dias, 1121170
 *
 * Created on 9 de Novembro de 2014, 0:20
 */

#ifndef MENU_H
#define	MENU_H

#include <cstdlib>
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

void printLimSUP(int size);
void printLimINF(int size);
int printMenuINICIAL();
void printInfo();
string askNome();
string askNomeFicheiro();
int createArmazemMenu();
int createGrafoMenu();
int askVertex(int indice);

#endif	/* MENU_H */

