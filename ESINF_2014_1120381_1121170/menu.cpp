/* 
 * File:   menu.cpp
 * Author: Francisco Amado, 1120381 e Luciano Dias, 1121170
 *
 * Created on 8 de Novembro de 2014, 10:48
 */

#include "menu.h"

/*
 * 
 */
void printLimSUP(int size) {
    cout << '|';
    for (int i = 0; i < size - 2; i++) {
        cout << ':';
    }
    cout << '|' << endl;
}

/*
 * 
 */
void printLimINF(int size) {
    cout << '|';
    for (int i = 0; i < size - 2; i++) {
        cout << ':';
    }
    cout << '|' << endl;
}

int printMenuINICIAL() {

    string prt = "|Bem vindo ao Simulador de Armazem";
    int resp = 0;

    printLimSUP(prt.size());

    cout << prt << endl;
    cout << "|1. Criar um Armazem" << endl;
    cout << "|2. Importar um armazem" << endl;
    cout << "|3. Info" << endl;
    cout << "|0. Sair" << endl;

    printLimINF(prt.size());

    cin >> resp;

    return resp;
}

void printInfo() {

    string ptr = "|::::::::::Info::::::::::|";

    cout << endl;
    cout << ptr << endl;
    cout << "|Autores:" << endl;
    cout << "|1120381, Francisco Amado" << endl;
    cout << "|1121170, Luciano Dias" << endl;
    printLimINF(ptr.size());
    cout << endl;
}

string askNome() {
    string ptr;
    cout << "Nome do produto?   ";
    cin >> ptr;
    return ptr;
}

string askNomeFicheiro() {
    string ptr;
    cout << "Nome do ficheiro?  ";
    cin >> ptr;
    return ptr;
}

int createArmazemMenu() {
    string prt = "|Armazem";
    int resp = 0;

    printLimSUP(prt.size());

    cout << prt << endl;
    cout << "|1. Info do Armazem" << endl;
    cout << "|2. Inserir um produto fresco" << endl;
    cout << "|3. Inserir um produto normal" << endl;
    cout << "|4. Inserir varios produtos frescos" << endl;
    cout << "|5. Inserir varios produtos frescos" << endl;
    cout << "|6. Eliminar um produto fresco" << endl;
    cout << "|7. Eliminar um produto normal" << endl;
    cout << "|8. Imprimir para ficheiro de texto" << endl;
    cout << "|9. Menu do grafo" << endl;
    cout << "|0. Sair" << endl;

    printLimINF(prt.size());

    cin >> resp;

    return resp;
}

int createGrafoMenu() {
    string ptr = "|Grafo";

    int resp = 0;

    printLimSUP(ptr.size());
    cout << ptr << endl;
    cout << "|1. Construir o grafo" << endl;
    cout << "|2. Apresentar todos os percursos possiveis entre dois depositos" << endl;
    cout << "|3. Apresentar um percurso entre dois depositos do mesmo tipo" << endl;
    cout << "|4. Calcular o percurso mais curto entre dois depositos" << endl;
    cout << "|5. Imprimir o grafo" << endl;
    cout << "|0. Sair" << endl;
    printLimINF(ptr.size());

    cin >> resp;

    return resp;
}

int askVertex(int i) {
    int ptr = 0;
    cout << i << "o vertice?  ";
    cin >> ptr;
    return ptr;
}