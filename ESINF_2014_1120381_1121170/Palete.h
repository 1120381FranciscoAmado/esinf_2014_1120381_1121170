/* 
 * File:   Palete.h
 * Author: Francisco Amado, 1120381 e Luciano Dias, 1121170
 *
 * Created on 22 de Outubro de 2014, 17:16
 */

#ifndef PALETE_H
#define	PALETE_H

#include <iostream>
#include <string>

using namespace std;

class Palete {
private:
    string nome;
    int capacidadeMaxima;
    int actual;
public:
    Palete();
    Palete(string nome, int capacidadeMaxima);
    Palete(const Palete &p);
    virtual ~Palete();

    void setNome(string n);
    void setCapacidadeMax(int capMax);
    void setActual(int atual);

    string getNome() const;
    int getCapacidadeMax() const;
    int getActual() const;

    void incrementarActual();
    void decrementarActual();

    bool operator==(const Palete& p);
    bool operator!=(const Palete &p);
    Palete & operator=(const Palete &p);
    void write(ostream& out) const;
    friend ostream& operator<<(ostream& out, const Palete& p);

};

Palete::Palete() {
    nome = "Sem nome definido";
    actual = 0;
}

Palete::Palete(string nome, int capacidadeMaxima) {
    this->nome = nome;
    this->capacidadeMaxima = capacidadeMaxima;
    ++actual;
}

Palete::Palete(const Palete &p) {
    this->nome = p.nome;
    this->capacidadeMaxima = p.capacidadeMaxima;
    actual = p.actual;
}

Palete::~Palete() {
}

string Palete::getNome() const {
    return nome;
}

int Palete::getCapacidadeMax() const {
    return capacidadeMaxima;
}

int Palete::getActual() const {
    return actual;
}

void Palete::setNome(string n) {
    nome = n;
}

void Palete::setActual(int atual) {
    this->actual = actual;
}

void Palete::setCapacidadeMax(int capMax) {
    capacidadeMaxima = capMax;
}

void Palete::incrementarActual() {
    actual++;
}

void Palete::decrementarActual() {
    actual--;
}

bool Palete::operator!=(const Palete& p) {
    if (this->nome == p.nome) {
        if (this->actual == p.actual) {
            return false;

        }
    }
    return true;
}

bool Palete::operator==(const Palete& p) {
    if (this->nome == p.nome) {
        if (this->actual == p.actual) {
            return true;
        }
    }
    return false;
}

Palete& Palete::operator=(const Palete& p) {
    this->nome = p.nome;
    this->capacidadeMaxima = p.capacidadeMaxima;
    this->actual = p.actual;
    return *this;
}

void Palete::write(ostream& out) const {
    cout << "Nome: " << nome << "\n" << endl;

}

ostream & operator<<(ostream &out, const Palete &p) {
    p.write(out);
    return out;
}


#endif	/* PALETE_H */

